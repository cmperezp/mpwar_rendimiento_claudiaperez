# MPWAR_Rendimiento_ClaudiaPerez

Para la instalación por favor realizar los siguientes pasos:
1. Ingresar a la carpeta vagrant del directorio raíz de este proyecto
2. Ejecutar vagrant up
3. Luego de tener la máquina virtual arriba ejecutar el comando vagrant ssh services
4. Ir al directorio /etc/redis
5. editar con usuario administrador el archivo 6379.conf
6. Editar en la linea 8 el valor de bind, cambiar 127.0.0.1 por 0.0.0.0
7. Salvar este cambio
8. Salir de esta máquina con el comando exit
9. Ejecutar el comando vagrant halt
10. Ejecutar el comando vagrant up
11. Ejecutar el comando vagrant ssh
12. Dirigirse al directorio /var/www/symfony
13. Ejecutar el comando:
    php composer.phar update
13. Ejecutar el consumer de rabbit con el siguiente comando:
    php bin/console rabbitmq:multiple-consumer upload_image
14. Ingresar en un navegador a: http://192.168.50.20/

Nota: El proyecto también puede ser clonado desde el repositorio publico:
git clone https://cmperezp@bitbucket.org/cmperezp/mpwar_rendimiento_claudiaperez.git

# Análisis Blackfire
1. Luego de implementar el guardado de los mensajes en redis, se realiza el análisis en Black fire y no se realiza ningún cambio ya que las clases que 
mas tiempo demoran son o propias de symfony, como el eventDispatcher o de php.
https://blackfire.io/profiles/c8571bdb-4225-490d-ae45-ef25e1ecb862/graph 

![Black Fire Redis](assets/blackfire/BlackFire-Redis.png "Redis")

2. Luego de la implementación cd Elastic y del filtro de cambiar el color a negativo
se observa que sigue sin haber cambios para realizar a nivel personalizado ya que el tiempo lo consume en su 
mayoría clases propias de symfony

![Black Fire Elastic](assets/blackfire/BlackFire-Elastic.png "Elastic")
