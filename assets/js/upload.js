import Dropzone from "dropzone/dist/dropzone"
import '../css/upload.scss'

Dropzone.autoDiscover = false;

const dropzone = $("div#uploadImage");

dropzone.dropzone({
    url: $('.uploader').data('upload-image'),
    paramName: "file",
    parallelUploads: 100,
    method: "POST",
    maxFilesize: 10,
    maxFiles: 5,
    clickable: true,
    acceptedFiles: '.jpg, .jpeg, .png, .gif, .pdf',
    addRemoveLinks: true,
    dictFileTooBig: "Archivo es muy grande. Tamaño permitido {{maxFilesize}}mb",
    dictInvalidFileType: "Tipo de archivo inválido",
    dictCancelUpload: "Cancel",
    dictRemoveFile: "Remove",
    dictMaxFilesExceeded: "Maximo de archivos permitidos: {{maxFiles}} ",
    dictDefaultMessage: "Arrastra las imágenes aquí: ",

});

