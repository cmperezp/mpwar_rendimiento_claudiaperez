<?php

namespace App\Services;


use Predis\Client;
use Symfony\Component\Serializer\Serializer;

class RedisCache
{

    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }


    public function get($key)
    {
        $this->client->get($key);
    }


    public function invalidate($key)
    {
        $this->client->del(array($key));
    }


    public function set($key, $value, $ttl = 0)
    {
        if($ttl >0) {
            $this->client->setex($key, $ttl, $value);
        }else{
            $this->client->set($key, $value);
        }
    }

}