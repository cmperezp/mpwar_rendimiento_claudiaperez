<?php

declare(strict_types=1);

namespace App\Services;


use App\Entity\Image;
use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelInterface;

final class UploadTransformService implements ConsumerInterface
{


    private $eventDispatcher;
    private $kernel;

    public function __construct(EventDispatcherInterface $eventDispatcher, KernelInterface $kernel)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->kernel          = $kernel;
    }

    public function execute(AMQPMessage $msg)
    {


        $imageResponse = json_decode($msg->body, true);

        $generalPath = __DIR__ . '/../';


        $name   = $imageResponse['name'];
        $height = $imageResponse['height'];
        $width  = $imageResponse['width'];
        $uid    = $imageResponse['uuid'];
        $tag    = $imageResponse['tag'];
        $path   = $generalPath . $imageResponse['path'] .  $imageResponse['name'];
        $newPath = $generalPath . $imageResponse['path'];
        $filter = 'transform';

        $image  = new ImageResize($path);
        $this->resizeImage($image, $newPath, $name, $uid, $height, $width, $filter);

        $imageToSave = new Image($uid, $name, $newPath, $tag, $width, $height, $filter);

        $container = $this->kernel->getContainer();

        $listener = new UploadEvent($container, $imageToSave);

        $this->eventDispatcher->dispatch(
            'file.save',
            $listener
        );
    }

    private function resizeImage(ImageResize $image, string $path, string $name, string $uid, int $height, int $width, string $filter): string
    {

        $image->resizeToHeight($height);
        $image->resizeToWidth($width);

        try {
            $image->save($path . '/' . $uid . $filter . $name);
        } catch (ImageResizeException $e) {
        }


        return $path;
    }
}