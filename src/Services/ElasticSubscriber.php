<?php

namespace App\Services;


use App\Entity\Image;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;


final class ElasticSubscriber implements EventSubscriber
{

    public function getSubscribedEvents()
  {
    return [
      'postPersist',
      'postUpdate',
    ];
  }

    public function postUpdate(LifecycleEventArgs $args)
  {

    $entity = $args->getEntity();
    if ($entity instanceof Image) {
      $this->handleEvent($entity);
    }
  }


  public function postPersist(LifecycleEventArgs $args)
  {

    $imageEntry = $args->getObject();

    $imagePersist = [
      'id'          => $imageEntry->getId(),
      'uuid'        => $imageEntry->getUid(),
      'name'        => $imageEntry->getName(),
      'path'        => $imageEntry->getPath(),
      'height'       => $imageEntry->getHeight(),
      'filter'      => $imageEntry->getFilter(),
      'tag'        => $imageEntry->getTag(),
      'width' => $imageEntry->getwidth(),
    ];
    $hosts = [
      '192.168.50.100:9200',
    ];
    $elasticSearch = ClientBuilder::create()
      ->setHosts($hosts)
      ->build();

    $this->persistElasticSearch($imagePersist, $elasticSearch);
  }

    private function persistElasticSearch(array $json, Client $elasticSearch): void
  {


    $params = [
      'index' => 'imagenes',
      'type'  => 'image',
      'id'    => $json['id'],
      'body'  => [
        'tag'        => $json['tag'],
        'filter'      => $json['filter']
      ],
    ];


    $elasticSearch->index($params);
  }

    private function handleEvent(Image $image)
  {
    $hosts = [
      '192.168.50.100:9200',
    ];

    $elasticSearch = ClientBuilder::create()
      ->setHosts($hosts)
      ->build();
    $this->persistUpdateOnElasticSearch($image, $elasticSearch);
  }

    private function persistUpdateOnElasticSearch(Image $image, Client $elasticSearch): void
  {
    $tags   = implode(',', $image->tags()->value());

    $params = [
      'index' => 'imagenes',
      'type'  => 'image',
      'id'    => $image->getId(),
      'body'  => [
        'tags'        => $image->getTag(),
        'filter'      => $image->getFilter(),
      ],
    ];

    $elasticSearch->index($params);
  }

}
