<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

final class UploadEvent extends Event
{

    private $container;
    private $image;

    public function __construct(ContainerInterface $container, Image $image)
    {
        $this->container = $container;
        $this->image     = $image;
    }

    public function uploadImageTransform()
    {
        $message = [
            "id" => $this->image->getId(),
            "uuid" => $this->image->getUid(),
            "path" => $this->image->getPath(),
            "name" => $this->image->getName(),
            "tag" => $this->image->getTag(),
            "width" => $this->image->getWidth(),
            "height" => $this->image->getHeight(),
            "filter" => $this->image->getFilter(),

        ];

        $messageToRabbit = json_encode($message);
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->setContentType('application/json');
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->publish($messageToRabbit, 'KeyTransform');
    }

    public function uploadImageSave()
    {

        $message = [
            "id" => $this->image->getId(),
            "uuid" => $this->image->getUid(),
            "path" => $this->image->getPath(),
            "name" => $this->image->getName(),
            "tag" => $this->image->getTag(),
            "width" => $this->image->getWidth(),
            "height" => $this->image->getHeight(),
            "filter" => $this->image->getFilter(),
        ];

        $messageToRabbit = json_encode($message);
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->setContentType('application/json');
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->publish($messageToRabbit, 'KeySave');
    }

    public function uploadImageFilter()
    {

        $message = [
            "id" => $this->image->getId(),
            "uuid" => $this->image->getUid(),
            "path" => $this->image->getPath(),
            "name" => $this->image->getName(),
            "tag" => $this->image->getTag(),
            "width" => $this->image->getWidth(),
            "height" => $this->image->getHeight(),
            "filter" => $this->image->getFilter(),
        ];

        $messageToRabbit = json_encode($message);
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->setContentType('application/json');
        $this->container->get('old_sound_rabbit_mq.upload_image_producer')->publish($messageToRabbit, 'KeyFilter');
    }
}