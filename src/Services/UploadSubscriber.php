<?php
declare(strict_types = 1);

namespace App\Services;


use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class UploadSubscriber implements EventSubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return [
            'file.upload' => [

                [
                    'resizing'

                ],
                [
                    'filter'

                ],
                [
                    'save'
                ]
            ],
            'file.save' => [
                [
                    'save'
                ]
            ],
        ];
    }

    public function resizing(UploadEvent $publish)
    {
        $publish->uploadImageTransform();

    }
    public function save(UploadEvent $publish)
    {
        $publish->uploadImageSave();

    }
    public function filter(UploadEvent $publish)
    {
        $publish->uploadImageFilter();

    }


}