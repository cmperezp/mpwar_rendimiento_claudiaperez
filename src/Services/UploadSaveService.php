<?php
declare(strict_types=1);

namespace App\Services;


use App\Entity\Image;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Doctrine\ORM\EntityManager;
use Predis\Client;
use Symfony\Bundle\SecurityBundle\Tests\Functional\Bundle\JsonLoginBundle\Security\Http\JsonAuthenticationFailureHandler;

class UploadSaveService implements ConsumerInterface
{

    private $entityManager;
    private $client;

    public function __construct(EntityManager $entityManager, Client $client)
    {
        $this->entityManager = $entityManager;
        $this->client        = $client;
    }

    public function execute(AMQPMessage $msg)
    {

        $response = json_decode($msg->body, true);


        $image = new Image($response['uuid'], $response['name'], $response['path'], $response['tag'], $response['width'], $response['height'], $response['filter']);

        $this->entityManager->persist($image);
        $this->entityManager->flush();

        $subscription = new RedisCache($this->client);
        $msgToRedis = json_encode(array(
            'uuid' => $response['uuid'],
            'name' => $response['name'],
            'path' => $response['path'],
            'tag' => $response['tag'],
            'width' => $response['width'],
            'height' => $response['height'],
            'filter' => $response ['filter'],
        ));

        $subscription->set((string) $image->getId(), $msgToRedis);
        $subscription->invalidate('allimages');
    }
}