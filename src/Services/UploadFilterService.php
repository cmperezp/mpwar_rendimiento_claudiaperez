<?php
/**
 * Created by IntelliJ IDEA.
 * User: claudiaperez
 * Date: 17/6/18
 * Time: 14:20
 */

namespace App\Services;


use App\Entity\Image;
use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\KernelInterface;

final class UploadFilterService implements ConsumerInterface
{


  private $eventDispatcher;
  private $kernel;

  public function __construct(EventDispatcherInterface $eventDispatcher, KernelInterface $kernel)
  {
    $this->eventDispatcher = $eventDispatcher;
    $this->kernel          = $kernel;
  }


  public function execute(AMQPMessage $msg)
  {
    $imageResponse = json_decode($msg->body, true);

    $generalPath = __DIR__ . '/../';


    $name   = $imageResponse['name'];
    $height = $imageResponse['height'];
    $width  = $imageResponse['width'];
    $uid    = $imageResponse['uuid'];
    $tag    = $imageResponse['tag'];
    $path   = $generalPath . $imageResponse['path'] .  $imageResponse['name'];
    $newPath = $generalPath . $imageResponse['path'];
    $filter = 'negate';

    $image  = new ImageResize($path);
    $this->filterImage($image, $newPath, $name, $uid, $filter);

    $imageToSave = new Image($uid, $name, $newPath, $tag, $width, $height, $filter);

    $container = $this->kernel->getContainer();

    $listener = new UploadEvent($container, $imageToSave);

    $this->eventDispatcher->dispatch(
      'file.save',
      $listener
    );
  }

  private function filterImage(ImageResize $image, string $path, string $name, string $uid, string $filter): string
  {
    $image->resizeToHeight(200);
    $image->resizeToWidth(200);
    $image->addFilter(
      function ($imageDesc) {
        imagefilter($imageDesc, IMG_FILTER_NEGATE);
      }
    );

    try {
      $image->save($path . '/' . $uid . $filter . $name);
    } catch (ImageResizeException $e) {
    }


    return $path;
  }
}
