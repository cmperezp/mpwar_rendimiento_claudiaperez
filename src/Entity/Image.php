<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Column;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
final class Image
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $uid;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tag;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $width;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $height;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filter;

    public function __construct($uid, $name, $path, $tag, $width ,$height, $filter)
    {
        $this->uid  = $uid;
        $this->name = $name;
        $this->path = $path;
        $this->tag  = $tag;
        $this->width = $width;
        $this->height = $height;
        $this->filter = $filter;
    }


    public function getId()
    {
        return $this->id;
    }


    public function setId($id): void
    {
        $this->id = $id;
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function setUid($uid): void
    {
        $this->uid = $uid;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path): void
    {
        $this->path = $path;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function setTag($tag): void
    {
        $this->tag = $tag;
    }


    public function getWidth()
    {
        return $this->width;
    }


    public function setWidth($width): void
    {
        $this->width = $width;
    }


    public function getHeight()
    {
        return $this->height;
    }


    public function setHeight($height): void
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * @param mixed $filter
     */
    public function setFilter($filter): void
    {
        $this->filter = $filter;
    }



}