<?php

declare(strict_types=1);

namespace App\Repository;


use App\Entity\Image;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ImageRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Image::class);
    }

    public function findAll()
    {
        $returnAll =
            $this->createQueryBuilder('img')
                ->select('img.id')
                ->getQuery();

        return $returnAll->execute();
    }


}