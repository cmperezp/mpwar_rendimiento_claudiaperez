<?php

namespace App\Controller;

use Elasticsearch\ClientBuilder;
use App\Entity\Image;
use App\Services\RedisCache;
use Predis\Client;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

final class ShowImagesController extends Controller
{

    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getAllImages(Request $request)
    {
        $query  = $request->query->get('buscarElastic');


        $redis        = new RedisCache($this->client);
        $images= $this->requestSearch($query, $redis);

        return $this->render('images.html.twig',
            [
                'images' => $images,
            ]);
    }

    private function imagesToArray(array $listImages, RedisCache $redis): array
    {
        $images = [];
        foreach ($listImages as $imageId) {
            $id = (string) $imageId;

            if ($redis->get($id)) {
                $image = json_decode($redis->get($id), true);
            } else {
                $repository = $this->getDoctrine()->getRepository(Image::class);;
                $results    = $repository->find($id);
                $redis->set($id, json_encode($results));
            }

            $uuid = $image['uuid'];

            $images[$uuid][$id] = $image;
        }

        return $images;
    }
    private function indexElastic()
    {
        $indexParams['index'] = 'files';

        $hosts = [
            '192.168.50.100:9200',
        ];

        $client = ClientBuilder::create()
                  ->setHosts($hosts)
                      ->build();

        $params = [
            'index' => 'files'
        ];

        if (!$client->indices()->exists($indexParams)) {
            $client->indices()->create($params);

        }
    }

    private function searchOnElastic($query): array
    {
        $params = [
            'index' => 'files',
            'body'  => [
                "query" => [
                    "query_string" => [
                        "query"                               => $query,
                        "auto_generate_synonyms_phrase_query" => false,
                    ],
                ],

            ],
        ];

        $hosts = [
            '192.168.50.100:9200',
        ];
        $client  = ClientBuilder::create()
            ->setHosts($hosts)
            ->build();

        $results = $client->search($params);

        $images = [];
        foreach ($results['hits']['hits'] as $image) {
            array_push($images, (int) $image['_id']);
        }

        return $images;
    }
    private function requestSearch($query, $redis): array
    {
        if (!is_null($query)) {
            $this->indexElastic();
            $listImages = $this->searchOnElastic($query);
            $imagesRedis     = $this->imagesToArray($listImages, $redis);

        } else {

            $redis        = new RedisCache($this->client);
            $imageRepo    = $this->getDoctrine()->getRepository(Image::class);
            $imagesRedis  = [];
            if ($redis->get('allImages')) {

                $imagesToShow = $redis->get('allImages');
            } else {

                $imagesToShow = $imageRepo->findAll();
                $redis->set('allImages', json_encode($imagesToShow));

            }

            foreach ($imagesToShow as $imagesById) {
                $imageId = (string)$imagesById['id'];

                if ($redis->get($imageId)) {
                    array_push($imagesRedis, json_decode($redis->get($imageId)));
                } else {

                    array_push($imagesRedis, $imageRepo->find($imageId));


                    $redis->set($imageId, json_encode($imagesRedis));
                }

            };

        }

        return $imagesRedis;
    }
}