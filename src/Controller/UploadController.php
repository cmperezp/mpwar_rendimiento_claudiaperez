<?php

declare(strict_types = 1);

namespace App\Controller;

use App\Entity\Image;
use App\Services\UploadEvent;
use Doctrine\DBAL\Connection;
use Gumlet\ImageResize;
use Gumlet\ImageResizeException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;


final class UploadController extends Controller
{

    private $eventDispatcher;

    private $connection;

    public function __construct(EventDispatcherInterface $eventDispatcher, Connection $connection)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->connection = $connection;

    }

    public function index()
    {

        $response = $this->render('base.html.twig');

        return $response;
    }

    public function upload()
    {

        $storeFolder = '../public/uploads/';

        if (!empty($_FILES)) {

            $tempFile = $_FILES['file']['tmp_name'];

            $newFolder = $storeFolder . basename($_FILES['file']['name']);

            move_uploaded_file($tempFile, $newFolder);

            $name   = basename($_FILES['file']['name']);
            $uid    = $this->generarUuid();
            $path   = $storeFolder;
            $tag    = explode(".",basename($_FILES['file']['name']))[0];
            $width  = 200;
            $height = 150;
            $filter = '';

            $image  = new Image($uid, $name, $path, $tag, $width, $height, $filter);

            $container = $this->container;

            $listener = new UploadEvent($container, $image);


            $this->eventDispatcher->dispatch(
                'file.upload',
                $listener
            );

        }

        return new Response();
    }

    private function generarUuid()
    {

        try {
            $uuid4 = Uuid::uuid4();
            return $uuid4;

        } catch (UnsatisfiedDependencyException $e) {

            echo 'Caught exception: ' . $e->getMessage() . "\n";

        }


    }
}
